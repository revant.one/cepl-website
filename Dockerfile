FROM nginx:stable
COPY . /usr/share/nginx/html
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf
